# README #

**watteRson** is an `R` package for Slatkin's implementation of the Ewens-Watterson test of neutrality on the allelic configuration for a locus. You can install directly from `R` with the following commands.

```r
install.packages('devtools')
devtools::install_gitlab('gregblomquist/watteRson')
```

Once installed, just load it like any other package.

```r
library(watteRson)
```